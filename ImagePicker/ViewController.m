//
//  ViewController.m
//  ImagePicker
//
//  Created by admin on 15/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
@import MobileCoreServices;

@interface ViewController ()

@end

@implementation ViewController{
    
    UIImage *image;
    UIImagePickerController *imgPicker;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(200, 350, 200, 200)];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonTapped)forControlEvents:UIControlEventTouchUpInside];
        [button setTitle : @"Image" forState:UIControlStateNormal];
        button.layer.cornerRadius = 5;
        button.backgroundColor = [UIColor colorWithRed:33/255.0f green:112/255.0f blue:141/255.0f alpha:1.0f];
        button.frame=CGRectMake(190, 370, 170, 50);
        [self.view addSubview:button];
    

    
    imgPicker = [[UIImagePickerController alloc]init];
    imgPicker.delegate = self;
    imgPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imgPicker.mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage,nil];
    [self presentViewController:imgPicker animated:YES completion:nil];
    
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
   
    if ([[info valueForKey:UIImagePickerControllerMediaType]isEqualToString:@"public.image"]) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(20, 50, 200, 200);
    [self.view addSubview:imageView];
    
    [imgPicker dismissViewControllerAnimated:YES completion:nil];
}

-(void)buttonTapped{
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [self.view addSubview:imageView];
    [self presentViewController:imgPicker animated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [imgPicker dismissViewControllerAnimated:YES completion:nil];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
